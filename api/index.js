const customExpress = require("./config/customExpress");
const app = customExpress();
const mongoose = require("mongoose");
require("dotenv").config();
const port = 666;

mongoose
  .connect(process.env.CONNECTIONSTRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connection Open!!");
  })
  .catch((err) => {
    console.log("Error");
  });

app.listen(port, () => {
  console.log(`API rodando na porta ${port}`);
});
